from django.forms import ModelForm, Form
from todos.models import TodoList, TodoItem
from django.views.generic.edit import UpdateView
from pydoc import describe
from django.db import models
from django.conf import settings
from django import forms

class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
                'name',
            ]

class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            'task',
            'due_date',
            'is_completed',
            'list'
        ]