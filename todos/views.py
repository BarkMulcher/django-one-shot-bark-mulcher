from django.shortcuts import render
from django import forms
from todos.forms import TodoListForm, TodoItemForm
from todos.models import TodoList, TodoItem
from django.shortcuts import get_object_or_404, render, redirect


# Create your views here.
def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list
    }
    return render(request, "todos/index.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }

    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        # validate inputs & save
        if form.is_valid():
            todolist = form.save()
            todolist.save()
            return redirect("todo_list_detail", id=todolist.id)

    else:
        form = TodoListForm()
    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()

            return redirect('todo_list_detail', id=todolist.id)

    else:
        form = TodoListForm()
    context = {
        'form': form
    }

    return render(request, 'todos/edit.html', context)

def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == 'POST':
        todolist.delete()
        return redirect('todo_list_list')

    return render(request, 'todos/delete.html')

def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        'form': form
    }

    return render(request, 'todos/create_item.html', context)

def todo_item_update(request, id):
    updated_list_item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=updated_list_item)
        if form.is_valid():
            updated_item = form.save()
            return redirect('todo_list_detail', id=updated_item.list.id)
    else:
        form = TodoItemForm()
    context = {
        'form': form
    }
    return render(request, 'todos/update_item.html', context)
